import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppHomeComponent } from './app-home/app-home.component';
import { AppInicioJogoComponent } from './app-inicio-jogo/app-inicio-jogo.component';
import { AppJogoComponent } from './app-jogo/app-jogo.component';
import { AppRankingComponent } from './app-ranking/app-ranking.component';
const routes: Routes = [
  {path:"",component:AppHomeComponent},
  { path:'home', component:AppHomeComponent},
  { path:'inicio-jogo', component:AppInicioJogoComponent},
  { path:'jogo', component:AppJogoComponent},
  { path:'ranking', component:AppRankingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }
