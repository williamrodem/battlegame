import { TestBed } from '@angular/core/testing';

import { AppJogoService } from './app-jogo.service';

describe('AppJogoService', () => {
  let service: AppJogoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppJogoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
