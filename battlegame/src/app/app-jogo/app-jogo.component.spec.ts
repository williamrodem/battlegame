import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppJogoComponent } from './app-jogo.component';

describe('AppJogoComponent', () => {
  let component: AppJogoComponent;
  let fixture: ComponentFixture<AppJogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppJogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppJogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
