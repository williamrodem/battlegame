import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppInicioJogoComponent } from './app-inicio-jogo/app-inicio-jogo.component';
import { AppRankingComponent } from './app-ranking/app-ranking.component';
import { AppJogoComponent } from './app-jogo/app-jogo.component';
import { AppHomeComponent } from './app-home/app-home.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';

@NgModule({
  declarations: [
    AppComponent,
    AppJogoComponent,
    AppInicioJogoComponent,
    AppRankingComponent,
    AppHomeComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProgressbarModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
