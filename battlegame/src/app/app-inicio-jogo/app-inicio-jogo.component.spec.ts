import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppInicioJogoComponent } from './app-inicio-jogo.component';

describe('AppInicioJogoComponent', () => {
  let component: AppInicioJogoComponent;
  let fixture: ComponentFixture<AppInicioJogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppInicioJogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppInicioJogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
